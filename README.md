# Pycroscope v1.0
### Kieran Hymas
## Table of Contents
1. [Introduction](Introduction)
2. [Installation](Installation)
3. [Features](Features)
4. [Contact](Contact)
---

## Introduction

Some people enjoy the feeling of compressing their corneas against the cool eyepieces of their microscopes in order to get a glimpse of the fascinating microscopic world inhabiting their slides. I am not one of those people. I much prefer to plug the in-built webcam of my microscope into my computer and see a live image cast to the screen. There are a myriad of reasons why this is much nicer than staring down those binocular eye-pieces like some mad explorer on microbiological safari. For instance: the ability to take screen shots of precious micron-scale moments, adapting image brightness and contrast on the fly and in some cases using the power of your computer for image analysis!

Unfortunately for me, my new microscope came with viewing software on a CD (how perverse in this day and age ...) that was only suitable for a Windows PC. Unaware of any other microscope viewing software available for Linux, I proceeded to develop Pycroscope; microscope image viewing software written exclusively in Python (Py-croscope ... get it?). While the software is relatively bare-bones in the current version (v1.0), I hope to expand it in future versions making full use of the wonderful OpenCV library on which it is based.

## Installation

Currently there are no setup tools for Pycroscope, I hope to register it on PyPI at some point to make it a little easier for people to install but for now you'll have to do it manually. There are a few dependencies that must be met in order to run the program; you must have:

* Python3
* OpenCV
* imutils
* numpy
* Pillow

and that's it! The required modules can be easily be installed using pip (beware, make sure to remove PIL from your computer before installing Pillow).

## Features

After starting Pycroscope, begin a new stream by clicking 'File > New stream > source X' (where X is the index for your microscope webcam) to open a live video stream on your computer. You can then use the tools in the tool bar (or in the View and Tools drop down menus) to modify the image to your liking. There are various tools available to manipulate your image such as:

* Blur tool
* Colour modifier
* Paint brush
* Edge detection tool

and you can also take a snapshot or record the video stream and save your discoveries to disk!

## 4. Contact

You can contact me through email at kieranhymas93@gmail.com
