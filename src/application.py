#Imports
from videostream import *
import tkinter as tk
from tkinter import *
from tkinter import filedialog
from tkinter.colorchooser import *
from PIL import Image, ImageTk
import numpy as np
import cv2
import os
import time

#Classes
class Application:
    def __init__(self, window):
        self.wd = os.path.expanduser('~')
        self.window = window
        self.window.title("Pycroscope")
        self.width = self.window.winfo_screenwidth()
        self.height = self.window.winfo_screenheight()
        self.window.geometry(str(self.width) + "x0+0+0")

        #Videostream options
        self.videostreamplaying = False
        self.videostreampaused = False
        self.vid = 0 #Initialise an empty video VideoStream
        self.availableStreams = self.getPossibleVideoStreams()
        self.delay = 5
        #Image options
        self.options = {'bnw' : BooleanVar(), \
                        'vMirror' : False, \
                        'hMirror' : False, \
                        'Rotate' : 0, \
                        'Blur' : BooleanVar(), \
                        'BlurKernelh' : IntVar(), \
                        'BlurKernelw' : IntVar(), \
                        'BlurDeviationX' : IntVar(), \
                        'BlurDeviationY' : IntVar(), \
                        'Brightness' : IntVar(), \
                        'Contrast' : IntVar(), \
                        'Gamma' : DoubleVar(), \
                        'BrushSize' : DoubleVar(), \
                        'BrushColour' : [0, 0, 0], \
                        'PaintArray' : [], \
                        'LastPaintArray' : [], \
                        'EdgeDetection' : BooleanVar(), \
                        'EdgeDetectionColour' : [0, 255, 0], \
                        'EdgeDetectionThickness' : IntVar(), \
                        'EdgeDetectionThresh1' : DoubleVar(), \
                        'EdgeDetectionThresh2' : DoubleVar(), \
                        'EdgeDetectionMode' : StringVar(), \
                        'EdgeDetectionMethod' : StringVar() \
                        }

        #Toolbar options
        self.toolbarExists = False
        self.toolbaricons = [tk.PhotoImage(file="./assets/icons/snapshot.png"), \
                        tk.PhotoImage(file="./assets/icons/record.png"), \
                        tk.PhotoImage(file="./assets/icons/rotate_anti.png"), \
                        tk.PhotoImage(file="./assets/icons/rotate.png"), \
                        tk.PhotoImage(file="./assets/icons/vmirror.png"), \
                        tk.PhotoImage(file="./assets/icons/hmirror.png"), \
                        tk.PhotoImage(file="./assets/icons/blur.png"), \
                        tk.PhotoImage(file="./assets/icons/colour.png"), \
                        tk.PhotoImage(file="./assets/icons/brush.png"), \
                        tk.PhotoImage(file="./assets/icons/edge.png") \
                        ]

        #Recoring
        self.recordingWindowExists = False
        self.recordpaused = False
        self.recording = False
        self.framerate = IntVar()
        self.framerate.set(30)
        self.recordingicons = [tk.PhotoImage(file="./assets/icons/record_black.png"), \
                                tk.PhotoImage(file="./assets/icons/record_red.png"), \
                                tk.PhotoImage(file="./assets/icons/pause.png"), \
                                tk.PhotoImage(file="./assets/icons/stop.png") \
                                ]

        #Blur options
        self.blurOptionsExists = False

        #Colour options
        self.colourOptionsExists = False
        self.options['Contrast'].set(0)
        self.options['Brightness'].set(0)
        self.options['Gamma'].set(1)

        #Paintbrush options
        self.paintbrushOptionsExists = False
        self.painting = False
        self.painticons = [tk.PhotoImage(file="./assets/icons/brush.png"), \
                            tk.PhotoImage(file="./assets/icons/eraser.png"), \
                            tk.PhotoImage(file="./assets/icons/undo.png") \
                            ]

        #Edge detection options
        self.edgeDetectionWindowExists = False
        self.options['EdgeDetectionThresh1'].set(50)
        self.options['EdgeDetectionThresh2'].set(50)
        self.options['EdgeDetectionMode'].set("RETR_TREE")
        self.options['EdgeDetectionMethod'].set("CHAIN_APPROX_SIMPLE")

        #INIT
        self.initApp()
        self.window.mainloop()

    def __version__(self):
        return '1.0'

    def initApp(self):
        self.initMenubar()
        self.initToolbar()

    #Menubar functions
    def initMenubar(self):
        self.menubar = Menu(self.window)
        self.window.config(menu=self.menubar)

        self.filemenu = Menu(self.menubar, tearoff=0)
        self.menubar.add_cascade(label="File", menu=self.filemenu)
        self.sessionmenu = Menu(self.filemenu, tearoff=0)
        self.filemenu.add_cascade(label="New stream", menu=self.sessionmenu)
        for ind in self.availableStreams:
            self.sessionmenu.add_command(label="source {}".format(str(ind)), command=lambda stream=ind : self.launchVideo(stream))
        self.filemenu.add_command(label="Refresh stream list", command=self.refreshStreams)
        self.streamoptionsmenu = Menu(self.filemenu, tearoff=0)
        self.filemenu.add_cascade(label="Stream actions", state=DISABLED, menu=self.streamoptionsmenu)
        self.streamoptionsmenu.add_command(label="Pause", command=self.onPause)
        self.streamoptionsmenu.add_command(label="Resume", command=self.onResume, state=DISABLED)
        self.streamoptionsmenu.add_command(label="Kill", command=self.cleanUpVideo)
        self.filemenu.add_command(label="Exit", command=self.onExit)

        self.viewmenu = Menu(self.menubar, tearoff=0)
        self.viewmenu.add_command(label="Rotate (clockwise)", command=self.onRotate)
        self.viewmenu.add_command(label="Rotate (anti-clockwise)", command=self.onaRotate)
        self.viewmenu.add_command(label="Flip (vertical)", command=self.onvMirror)
        self.viewmenu.add_command(label="Flip (horizonal)", command=self.onhMirror)
        self.menubar.add_cascade(label="View", menu=self.viewmenu)

        self.toolmenu = Menu(self.menubar, tearoff=0)
        self.toolmenu.add_command(label="Snapshot", command=self.onSnapshot)
        self.toolmenu.add_command(label="Record video", command=self.onRecord)
        self.toolmenu.add_separator()
        self.toolmenu.add_command(label="Blur", command=self.onBlur)
        self.toolmenu.add_command(label="Colour", command=self.onColour)
        self.toolmenu.add_command(label="Brush", command=self.onBrush)
        self.toolmenu.add_command(label="Edge detection", command=self.onEdge)
        self.toolmenu.add_separator()
        self.toolmenu.add_command(label="Show toolbar", command=self.initToolbar)
        self.menubar.add_cascade(label="Tools", menu=self.toolmenu)

        self.helpmenu = Menu(self.menubar, tearoff=0)
        self.helpmenu.add_command(label="About", command=self.onAbout)
        self.menubar.add_cascade(label="Help", menu=self.helpmenu)

    #Toolbar functions
    def initToolbar(self):
        if self.toolbarExists:
            return

        self.toolbarExists = True
        toolbar = Toplevel(self.window, bd=1, relief=RAISED)
        toolbar.title("Pycroscope: Toolbar")
        toolbar.geometry("+" + str(self.window.winfo_screenwidth()) + "+" + str(self.window.winfo_screenheight() // 3))

        #load icons
        pair1 = Frame(toolbar)
        snapshotButton = Button(pair1, relief=FLAT, image=self.toolbaricons[0], command=self.onSnapshot)
        snapshotButton.pack(side=LEFT, padx=1, pady=1)
        snapshotButton.image = self.toolbaricons[0]
        recordButton = Button(pair1, relief=FLAT, image=self.toolbaricons[1], command=self.onRecord)
        recordButton.pack(side=LEFT, padx=1, pady=1)
        recordButton.image = self.toolbaricons[1]
        pair1.pack(side=TOP)

        toolbarsep1 = Frame(toolbar, relief=SUNKEN, bd=1, height=2)
        toolbarsep1.pack(side=TOP, padx=2, pady=2, fill=X)

        pair2 = Frame(toolbar)
        rotateantiButton = Button(pair2, relief=FLAT, image=self.toolbaricons[2], command=self.onaRotate)
        rotateantiButton.pack(side=LEFT, padx=1, pady=1)
        rotateButton = Button(pair2, relief=FLAT, image=self.toolbaricons[3], command=self.onRotate)
        rotateButton.pack(side=LEFT, padx=1, pady=1)
        pair2.pack(side=TOP)

        pair3 = Frame(toolbar)
        vmirrorButton = Button(pair3, relief=FLAT, image=self.toolbaricons[4], command=self.onvMirror)
        vmirrorButton.pack(side=LEFT, padx=1, pady=1)
        hmirrorButton = Button(pair3, relief=FLAT, image=self.toolbaricons[5], command=self.onhMirror)
        hmirrorButton.pack(side=LEFT, padx=1, pady=1)
        pair3.pack(side=TOP)

        toolbarsep2 = Frame(toolbar, relief=SUNKEN, bd=1, height=2)
        toolbarsep2.pack(side=TOP, padx=2, pady=2, fill=X)

        pair4 = Frame(toolbar)
        blurButton = Button(pair4, relief=FLAT, image=self.toolbaricons[6], command=self.onBlur)
        blurButton.pack(side=LEFT, padx=1, pady=1)
        colourButton = Button(pair4, relief=FLAT, image=self.toolbaricons[7], command=self.onColour)
        colourButton.pack(side=LEFT, padx=1, pady=1)
        pair4.pack(side=TOP)

        pair5 = Frame(toolbar)
        brushButton = Button(pair5, relief=FLAT, image=self.toolbaricons[8], command=self.onBrush)
        brushButton.pack(side=LEFT, padx=1, pady=1)
        edgeButton = Button(pair5, relief=FLAT, image=self.toolbaricons[9], command=self.onEdge)
        edgeButton.pack(side=LEFT, padx=1, pady=1)
        pair5.pack(side=TOP)

        toolbar.protocol("WM_DELETE_WINDOW", lambda arg=toolbar : self.onClosingToolbar(arg))

    def onClosingToolbar(self, widget):
        self.toolbarExists = False
        widget.destroy()

    #Recording
    def initRecordingWindow(self):
        if self.recordingWindowExists:
            return

        self.recordingWindowExists = True

        self.recordingWindow = Toplevel(self.window, bd=1)
        self.recordingWindow.title("Pycroscope: record video")

        self.recordingbuttonwidget = Frame(self.recordingWindow)
        self.recordButton = Button(self.recordingbuttonwidget, relief=FLAT, image=self.recordingicons[0], command=self.startRecording)
        self.pauseButton = Button(self.recordingbuttonwidget, relief=FLAT, image=self.recordingicons[2], command=self.pauseRecording)
        self.stopButton = Button(self.recordingbuttonwidget, relief=FLAT, image=self.recordingicons[3], command=self.stopRecording)

        self.recordButton.pack(side=LEFT)
        self.pauseButton.pack(side=LEFT)
        self.stopButton.pack(side=LEFT)
        self.recordingbuttonwidget.pack(side=TOP)

        self.frameratewidget = Frame(self.recordingWindow)
        self.frameratelabel = Label(self.frameratewidget, text="Frame rate", padx=5)
        self.frameratescale = Scale(self.frameratewidget, from_=5, to=60, orient=HORIZONTAL, variable=self.framerate)

        self.frameratelabel.pack(side=LEFT)
        self.frameratescale.pack(side=LEFT)

        self.frameratewidget.pack(side=TOP)

        self.recordingWindow.protocol("WM_DELETE_WINDOW", lambda arg=self.recordingWindow : self.onClosingRecordingWindow(arg))

    def startRecording(self):
        if not self.videostreamplaying:
            print('[-] Error: No videostream open to record from')
            return
        self.frameratescale.config(state=DISABLED)
        self.streamWriter = cv2.VideoWriter('Pycroscope_recording_{}.avi'.format(str(time.time())[:10]), cv2.VideoWriter_fourcc(*'MJPG'), self.framerate.get(), (int(self.vid.width), int(self.vid.height)), True)
        self.recording = True
        self.recordButton.config(relief=SUNKEN, image=self.recordingicons[1])

    def pauseRecording(self):
        if not self.recording:
            return
        if self.recordpaused:
            self.recordpaused = False
            self.pauseButton.config(relief=RAISED)
        else:
            self.recordpaused = True
            self.pauseButton.config(relief=SUNKEN)

    def stopRecording(self):
        if not self.recording:
            return
        self.recordButton.config(relief=RAISED, image=self.recordingicons[0])
        self.pauseButton.config(relief=RAISED)
        self.streamWriter.release()
        self.frameratescale.config(state=NORMAL)

    def onClosingRecordingWindow(self, widget):
        self.recordingWindowExists = False
        widget.destroy()

    #Blur options functions
    def initBlurOptionsWindow(self):
        if self.blurOptionsExists:
            return

        self.blurOptionsExists = True
        blurOptions = Toplevel(self.window, bd=1)
        blurOptions.title("Pycroscope: blur options")

        blurcheck = Checkbutton(blurOptions, text="Apply Guassian blur", variable=self.options['Blur'])
        blurcheck.pack(side=TOP)

        blurOptionsWidget = Frame(blurOptions)
        blurkernelhlabel = Label(blurOptionsWidget, text="Blur kernel height").grid(row=0)
        blurkernelllabel = Label(blurOptionsWidget, text="Blur kernel length").grid(row=1)
        blurdeviationXlabel = Label(blurOptionsWidget, text="Standard deviation X-axis").grid(row=2)
        blurdeviationYlabel = Label(blurOptionsWidget, text="Standard deviation Y-axis").grid(row=3)

        blurkernelhscale = Scale(blurOptionsWidget, from_=0, to=50, orient=HORIZONTAL, resolution=2, variable=self.options['BlurKernelh']).grid(row=0, column=1)
        blurkernellscale = Scale(blurOptionsWidget, from_=0, to=50, orient=HORIZONTAL, resolution=2, variable=self.options['BlurKernelw']).grid(row=1, column=1)
        blurdeviationXscale = Scale(blurOptionsWidget, from_=0, to=50, orient=HORIZONTAL, variable=self.options['BlurDeviationX']).grid(row=2, column=1)
        blurdeviationYscale = Scale(blurOptionsWidget, from_=0, to=50, orient=HORIZONTAL, variable=self.options['BlurDeviationY']).grid(row=3, column=1)
        blurOptionsWidget.pack(side=TOP)

        blurOptions.protocol("WM_DELETE_WINDOW", lambda arg=blurOptions : self.onClosingBlurOptions(arg))

    def onClosingBlurOptions(self, widget):
        self.blurOptionsExists = False
        widget.destroy()

    #Colour options functions
    def initColourOptionsWindow(self):
        if self.colourOptionsExists:
            return

        self.colourOptionsExists = True
        colourOptions = Toplevel(self.window, bd=1)
        colourOptions.title("Pycroscope: colour options")

        colourlessOptionsWidget = Frame(colourOptions)
        brightnesslabel = Label(colourlessOptionsWidget, text="Brightness").grid(row=0)
        contrastlabel = Label(colourlessOptionsWidget, text="Contrast").grid(row=1)

        brightnessscale = Scale(colourlessOptionsWidget, from_=-127, to=127, orient=HORIZONTAL, variable=self.options['Brightness']).grid(row=0, column=1)
        constrastscale = Scale(colourlessOptionsWidget, from_=-127, to=127, orient=HORIZONTAL, variable=self.options['Contrast']).grid(row=1, column=1)
        colourlessOptionsWidget.pack(side=TOP)

        bnwcheck = Checkbutton(colourOptions, text="Apply black and white filter", variable=self.options['bnw'])
        bnwcheck.pack(side=TOP)

        colourOptions.protocol("WM_DELETE_WINDOW", lambda arg=colourOptions : self.onClosingColourOptions(arg))

    def onClosingColourOptions(self, widget):
        self.colourOptionsExists = False
        widget.destroy()

    #Paintbrush options functions
    def initPaintbrushOptionsWindow(self):
        if self.paintbrushOptionsExists:
            return

        self.paintbrushOptionsExists = True
        self.paintbrushOptions = Toplevel(self.window, bd=1)
        self.paintbrushOptions.title("Pycroscope: paintbrush options")

        self.paintbrushToolsWidget = Frame(self.paintbrushOptions)
        self.brushButton = Button(self.paintbrushToolsWidget, relief=FLAT, image=self.painticons[0], command=self.onPaint)
        self.eraserButton = Button(self.paintbrushToolsWidget, relief=FLAT, image=self.painticons[1], command=self.onErase)
        self.undoButton = Button(self.paintbrushToolsWidget, relief=FLAT, image=self.painticons[2], command=self.onUndo)

        self.brushButton.grid(row=0, column=0)
        self.eraserButton.grid(row=0, column=1)
        self.undoButton.grid(row=0, column=2)
        self.paintbrushToolsWidget.pack(side=TOP)

        self.paintbrushSeparator = Frame(height=2, bd=1, relief=SUNKEN)
        self.paintbrushSeparator.pack(padx=5, pady=5, side=TOP)

        self.paintbrushOptionsWidget = Frame(self.paintbrushOptions)
        self.colourlabel = Label(self.paintbrushOptionsWidget, text="Brush colour").grid(row=0)
        self.sizelabel = Label(self.paintbrushOptionsWidget, text="Paintbrush size").grid(row=1)

        self.brushcolourbutton = Button(self.paintbrushOptionsWidget, bg=self.RBG2HTML(self.options['BrushColour']), command=self.setBrushColour)
        self.brushsizescale = Scale(self.paintbrushOptionsWidget, from_=5, to=25, orient=HORIZONTAL, variable=self.options['BrushSize'])

        self.brushcolourbutton.grid(row=0, column=1)
        self.brushsizescale.grid(row=1, column=1)
        self.paintbrushOptionsWidget.pack(side=TOP)

        self.paintbrushOptions.protocol("WM_DELETE_WINDOW", lambda arg=self.paintbrushOptions : self.onClosingPaintbrushOptions(arg))

    def onPaint(self):
        if not self.painting:
            self.painting = True
            self.brushButton.config(relief=SUNKEN)
        else:
            self.painting = False
            self.brushButton.config(relief=FLAT)

    def onErase(self):
        self.options['PaintArray'] = []

    def onUndo(self):
        try:
            self.options['PaintArray'].pop()
        except IndexError:
            pass

    def leftclick(self, event):
        if self.painting:
            x = int(self.canvas.winfo_pointerx() - self.canvas.winfo_rootx())
            y = int(self.canvas.winfo_pointery() - self.canvas.winfo_rooty())
            s = int(self.options['BrushSize'].get())
            c = tuple(self.options['BrushColour'])
            self.options['LastPaintArray'].append({'coords' : (x, y), 'size' : s, 'colour' : c})

    def releaseleft(self, event):
        if self.painting:
            self.options['PaintArray'].append(self.options['LastPaintArray'])
            self.options['LastPaintArray'] = []

    def setBrushColour(self):
        colour = askcolor(title='Pycroscope: brush colour', initialcolor=tuple(self.options['BrushColour']))
        if colour[0] == None:
            return
        self.options['BrushColour'][0] = int(colour[0][0])
        self.options['BrushColour'][1] = int(colour[0][1])
        self.options['BrushColour'][2] = int(colour[0][2])
        self.brushcolourbutton.config(bg=self.RBG2HTML(self.options['BrushColour']))

    def onClosingPaintbrushOptions(self, widget):
        self.paintbrushOptionsExists = False
        widget.destroy()

    #Edge Detection
    def initEdgeDetectionOptionsWindow(self):
        if self.edgeDetectionWindowExists:
            return

        self.edgeDetectionWindowExists = True
        self.edgedetectionOptions = Toplevel(self.window, bd=1)
        self.edgedetectionOptions.title("Pycroscope: edge detection options")

        self.edgedetectioncheck = Checkbutton(self.edgedetectionOptions, text="Activate edge detection", variable=self.options['EdgeDetection'])
        self.edgedetectioncheck.pack(side=TOP)

        self.edgedetectionOptionsWidget = Frame(self.edgedetectionOptions)
        self.edgecolourlabel = Label(self.edgedetectionOptionsWidget, text="Edge colour").grid(row=0)
        self.thicknesslabel = Label(self.edgedetectionOptionsWidget, text="Edge thickness").grid(row=1)
        self.threshold1label = Label(self.edgedetectionOptionsWidget, text="Threshold 1").grid(row=2)
        self.threshold2label = Label(self.edgedetectionOptionsWidget, text="Threshold 2").grid(row=3)
        self.edgemodelabel = Label(self.edgedetectionOptionsWidget, text="Mode").grid(row=4)
        self.edgemethodlabel = Label(self.edgedetectionOptionsWidget, text="Method").grid(row=5)

        self.edgecolourbutton = Button(self.edgedetectionOptionsWidget, bg=self.RBG2HTML(self.options['EdgeDetectionColour']), command=self.setEdgeColour)
        self.thicknessscale = Scale(self.edgedetectionOptionsWidget, from_=1, to=12, orient=HORIZONTAL, variable=self.options['EdgeDetectionThickness'])
        self.threshold1scale = Scale(self.edgedetectionOptionsWidget, from_=1, to=100, orient=HORIZONTAL, variable=self.options['EdgeDetectionThresh1'])
        self.threshold2scale = Scale(self.edgedetectionOptionsWidget, from_=1, to=100, orient=HORIZONTAL, variable=self.options['EdgeDetectionThresh2'])
        self.edgemodedropdown = OptionMenu(self.edgedetectionOptionsWidget, self.options['EdgeDetectionMode'], "RETR_LIST", "RETR_TREE", "RETR_CCOMP", "RETR_EXTERNAL")
        self.edgemethoddropdown = OptionMenu(self.edgedetectionOptionsWidget, self.options['EdgeDetectionMethod'], "CHAIN_APPROX_NONE", "CHAIN_APPROX_SIMPLE", "CHAIN_APPROX_TC89_L1", "CHAIN_APPROX_TC89_KCOS")

        self.edgecolourbutton.grid(row=0, column=1)
        self.thicknessscale.grid(row=1, column=1)
        self.threshold1scale.grid(row=2, column=1)
        self.threshold2scale.grid(row=3, column=1)
        self.edgemodedropdown.grid(row=4, column=1)
        self.edgemethoddropdown.grid(row=5, column=1)
        self.edgedetectionOptionsWidget.pack(side=TOP)

        self.edgedetectionOptions.protocol("WM_DELETE_WINDOW", lambda arg=self.edgedetectionOptions : self.onClosingEdgeDetectionOptions(arg))

    def setEdgeColour(self):
        colour = askcolor(title="Pycroscope: edge colour", initialcolor=tuple(self.options["EdgeDetectionColour"]))
        if colour[0] == None:
            return
        self.options['EdgeDetectionColour'][0] = int(colour[0][0])
        self.options['EdgeDetectionColour'][1] = int(colour[0][1])
        self.options['EdgeDetectionColour'][2] = int(colour[0][2])
        self.edgecolourbutton.config(bg=self.RBG2HTML(self.options['EdgeDetectionColour']))

    def onClosingEdgeDetectionOptions(self, widget):
        self.edgeDetectionWindowExists = False
        widget.destroy()

    #Menubar options
    #File
    def onExit(self):
        self.window.quit()

    #Help
    def onAbout(self):
        about = Toplevel(self.window)
        about.title("Pycroscope: About")

        self.logo = Image.open("./assets/logo/pycroscope_logo_128x128.png")
        self.logo_ph = ImageTk.PhotoImage(image=self.logo)
        iconframe = Label(about, image=self.logo_ph)
        iconframe.pack(side=LEFT, fill=BOTH, expand=YES)

        textframe = Frame(about, bg="#FFFFFF")
        titleframe = Text(textframe, height=1, width=15, highlightthickness=0, font=("Helvetica", 20, "bold"), relief=FLAT)
        titleframe.insert('1.0', "Pycroscope")
        titleframe.tag_configure("center", justify="center")
        titleframe.tag_add("center", 1.0, "end")
        titleframe.pack(side=TOP)
        titleframe.config(state=DISABLED)
        bodyframe = Text(textframe, height=5, width=20, highlightthickness=0, font=("Helvetica", 12), relief=FLAT)
        bodyframe.insert('1.0', "Author: Kieran Hymas\nYear: 2019\nVersion: {}\nLicense: GPLv2".format(self.__version__()))
        bodyframe.pack(side=TOP)
        bodyframe.config(state=DISABLED)
        textframe.pack(side=LEFT)

    #Toolbar functions
    def onSnapshot(self):
        #Deal with no open videostream
        if not self.videostreamplaying:
            print("[-] Error: no video stream open to screenshot.")
            return
        #Take snapshot
        ret, frame = self.vid.get_frame(self.options)
        if ret:
            newframe = cv2.cvtColor(frame,cv2.COLOR_RGB2BGR)
            path = tk.filedialog.asksaveasfilename(title="Pycroscope: save screenshot", defaultextension=".png", initialdir=self.wd)
            if path == () or path == '' or path == False:
                return
            self.wd = '/'.join(path.split('/')[:-1]) + '/'
            cv2.imwrite(path, newframe)
        else:
            print("[-] Error: something went wrong.")

    def onRecord(self):
        self.initRecordingWindow()

    def onRotate(self):
        self.options['Rotate'] = np.mod(self.options['Rotate'] + 1, 4)

    def onaRotate(self):
        self.options['Rotate'] = np.mod(self.options['Rotate'] - 1, 4)

    def onvMirror(self):
        self.options['vMirror'] = not self.options['vMirror']

    def onhMirror(self):
        self.options['hMirror'] = not self.options['hMirror']

    def onBlur(self):
        self.initBlurOptionsWindow()

    def onColour(self):
        self.initColourOptionsWindow()

    def onBrush(self):
        self.initPaintbrushOptionsWindow()

    def onEdge(self):
        self.initEdgeDetectionOptionsWindow()

    #Videostream functions
    def getPossibleVideoStreams(self):
        ind = 0
        arr = []
        for i in range(4):
            try:
                ret, cap = cv2.VideoCapture(ind).read()
                if ret:
                    print("[+] Found camera source: ", ind)
                    arr.append(ind)
                    ind += 1
                    cap.release()
                else:
                    ind += 1
            except:
                pass
        return arr

    def refreshStreams(self):
        if self.videostreamplaying:
            return #Put a pop up window here asking user if they want to close current stream to refresh devices
        self.sessionmenu.delete(0, len(self.availableStreams))
        self.availableStreams = self.getPossibleVideoStreams()
        for ind in self.availableStreams:
            self.sessionmenu.add_command(label=str(ind), command=lambda stream=ind : self.launchVideo(stream))

    def launchVideo(self, video_source):
        #Catch multiple instances of video source; this may be a feature in future versions but not now
        if self.videostreamplaying:
            print("[-] Error: videostream already open; close existing stream first")
            #Raise error window here asking if user wants to close existing session if 'yes' then call self.cleanUpVideo()
            return
        self.vid = VideoStream(video_source)
        self.videostreamplaying = True
        self.filemenu.entryconfig("Stream actions", state=NORMAL)
        self.newwin = Toplevel(self.window)
        self.newwin.title("Pycroscope: Video source {}".format(video_source))
        self.canvas = Canvas(self.newwin, width=self.vid.width, height=self.vid.height)
        self.canvas.bind("<Button-1>", self.leftclick)
        self.canvas.bind("<B1-Motion>", self.leftclick)
        self.canvas.bind("<ButtonRelease-1>", self.releaseleft)
        self.canvas.pack()
        self.updateVideo()
        self.newwin.protocol("WM_DELETE_WINDOW", self.cleanUpVideo)

    def updateVideo(self):
        if self.videostreamplaying and not self.videostreampaused:
            if self.vid.vidcap.isOpened():
                if self.painting:
                    self.canvas.config(cursor="cross black black")
                else:
                    self.canvas.config(cursor="arrow")
                ret, frame = self.vid.get_frame(self.options)
                if ret:
                    if self.recording and not self.recordpaused:
                        newframe = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
                        self.streamWriter.write(newframe)
                    self.photo = ImageTk.PhotoImage(image=Image.fromarray(frame))
                    self.canvas.create_image(0, 0, image=self.photo, anchor=NW)
                    self.window.after(self.delay, self.updateVideo)

    def onPause(self):
        self.videostreampaused = True
        self.streamoptionsmenu.entryconfig("Resume", state=NORMAL)
        self.streamoptionsmenu.entryconfig("Pause", state=DISABLED)

    def onResume(self):
        self.videostreampaused = False
        self.updateVideo()
        self.streamoptionsmenu.entryconfig("Resume", state=DISABLED)
        self.streamoptionsmenu.entryconfig("Pause", state=NORMAL)

    def cleanUpVideo(self):
        self.vid.__del__()
        self.newwin.destroy()
        self.videostreamplaying = False
        self.filemenu.entryconfig("Stream actions", state=DISABLED)
        self.options['PaintArray'] = []
        self.vid = 0

    #Misc functions
    def RBG2HTML(self, RBG):
        return "#%02x%02x%02x" % tuple(RBG)
